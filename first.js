/* Class definition */
var Person = function(name) {
    this.name = name;
}

/* Class method */
Person.say_hello = function() {
    console.log('Hello, class method!');
}

/* Instance method */
Person.prototype.myname = function() {
    console.log('My name is ' + this.name);
}

/* Creating object and testing methods */
var p = new Person('Alicia');
console.log(p.name);

p.myname();

Person.say_hello();

/* Inheritance example */
var Animal = function() {};
Animal.prototype.breath = function() {
    console.log('breath');
};

var Dog = function() {};
/* Dog inherits from Animal */
Dog.prototype = new Animal;

var dog = new Dog;
dog.breath();

/* New object following JSON format */
var Publisher = {
    pub: function() {
        console.log("I'm pub!")
    },
    sus: function() {
        console.log("I'm sus!")
    }
};

Publisher.pub();
Publisher.sus();

/* New object with two properties */
var MyObj = {'a': 1, 'b': 2};

console.log(MyObj.a);
console.log(MyObj.b);
